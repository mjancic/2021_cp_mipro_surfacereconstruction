/*---------------------------------------------------------------------------*\
  Local analytical model of dendrite tip growth
  The tip velocity is calculated by the 2D or 3D model of Cantor and Vogel
  The solution of the nonlinear equation is obtained by an hybrid
  newton/bisection method WHICH NEEDS TO BE CHANGED ...
\*---------------------------------------------------------------------------*/


#include <math.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <boost/math/special_functions/expint.hpp>
using namespace std;


//===================================================
//1D Newton Method combined with bisection
//adapted from C++ NUMERICAL RECIPES ($9.4)
//===================================================
template <class T>
double rtsafe(T &funcd, vector<double> paramMat, const double xinit, const double x1, const double x2, const double xacc, const double facc) {
  //Using a combination of Newton-Raphson and bisection, return the root of a
  //function bracketed between x1 and x2.
  //The root will be refined until its accuracy is known within +/-xacc.
  //funcd is a user-supplied struct that returns the function value as a functor
  //and the first derivative of the function at the point x as the function df (see text).

  int  iterMax=1000;            //Maximum allowed number of iterations.

  double xh,xl;
  double fl=funcd(x1,paramMat);
  double fh=funcd(x2,paramMat);
  if ((fl > 0.0 && fh > 0.0) || (fl < 0.0 && fh < 0.0))
    throw("Root must be bracketed in rtsafe");

  if (fl == 0.0) return x1;
  if (fh == 0.0) return x2;
  if (fl < 0.0) {                     //Orient the search so that f(xl) < 0.
      xl=x1;
      xh=x2;
    } else {
      xh=x1;
      xl=x2;
    }
  double rts=xinit;
  double dxold=abs(x2-x1);              //the “stepsize before last,”
  double dx=dxold;                      //and the last step.
  double f=funcd(rts,paramMat);
  double df=funcd.df(rts,paramMat);
  for (int j=0;j<iterMax;j++) {         //Loop over allowed iterations.
      if ((((rts-xh)*df-f)*((rts-xl)*df-f) > 0.0)   //Bisect if Newton out of range,
          || (abs(2.0*f) > abs(dxold*df))) {        //or not decreasing fast enough.
          dxold=dx;
          dx=0.5*(xh-xl);
          rts=xl+dx;
          if (xl == rts && abs(f) < facc)
            { return rts;
            }  //Change in root is negligible.
        } else {                      //Newton step acceptable. Take it.
          dxold=dx;
          dx=f/df;
          double temp=rts;
          rts -= dx;
          if (temp == rts && abs(f) < facc)
            { return rts;
            }
        }

      //double f=funcd(rts,paramMat); //BUG BEFORE 2014-05-23 corrected below
      f=funcd(rts,paramMat);

      if (abs(dx) < xacc && abs(f) < facc)
        { return rts;
        }

      //double df=funcd.df(rts,paramMat); //BUG BEFORE 2014-05-23 corrected below
      //No need to calculate df, since it is not used after this line
      //df=funcd.df(rts,paramMat);

      //The one new function evaluation per iteration.
      if (f < 0.0)                    //Maintain the bracket on the root.
        xl=rts;
      else
        xh=rts;

    }
  	cout << "Warning: Maximum number of iterations exceeded in rtsafe \n" << "Pe=" << rts << "  xl=" << xl << "  xh=" << xh << "  f=" << f << endl;

  // Result in case of Max iterations exceeded is the last value.
  return rts;

}




//*****************************************************************************
//***  CANTOR AND VOGEL 2D  ***************************************************
//*****************************************************************************

//===================================================
//Cantor And Vogel System Of Equation for 2D Dendrite
//===================================================
double systemCantorVogel2D(double Pe, vector<double> paramMat)
{ //double pi number (This is the maximum precision)
  const double pi = 3.1415926535897932384626433;
  //Construct system that must be f(Pe)=0 to obtain Cantor and Vogel Solution
  double f = paramMat[0] - sqrt(pi*Pe) * exp(Pe) * ( erfc(sqrt(Pe)) - erfc(sqrt(Pe*(1+paramMat[1]*Pe))) ) ;
  return f;
}
double systemCantorVogel2D_Jacobian(double Pe, vector<double> paramMat)
{ //double pi number (This is the maximum precision)
  const double pi = 3.1415926535897932384626433;
  //Jacobian matrix for Cantor and Vogel system
  double jac = 1 - exp(-paramMat[1]*Pe*Pe) * (1+2*paramMat[1]*Pe) /sqrt(1+paramMat[1]*Pe) - exp(Pe)*sqrt(pi)*(sqrt(Pe)+1/(2*sqrt(Pe))) *( erfc(sqrt(Pe)) - erfc(sqrt(Pe*(1+paramMat[1]*Pe))) ) ;
  return jac;
}
struct Funcd2D {
  double operator() (const double x, vector<double> paramMat)
    { return systemCantorVogel2D(x,paramMat); }
  double df(const double x, vector<double> paramMat)
    { return systemCantorVogel2D_Jacobian(x,paramMat); }
};

//===================================================
//Cantor And Vogel System Of Equation for 3D Dendrite
//===================================================
double systemCantorVogel3D(double Pe, vector<double> paramMat)
{ //Construct system that must be f(Pe)=0 to obtain Cantor and Vogel Solution
  double f = paramMat[0] - Pe * exp(Pe) * ( boost::math::expint(1,Pe) - boost::math::expint(1,Pe*(1+paramMat[1]*Pe))) ;
  return f;
}
double systemCantorVogel3D_Jacobian(double Pe, vector<double> paramMat)
{ //Jacobian matrix for Cantor and Vogel system
  double jac = 1 - exp(-paramMat[1]*Pe*Pe) * (1+2*paramMat[1]*Pe) /(1+paramMat[1]*Pe) - (1+Pe)*exp(Pe) * ( boost::math::expint(1,Pe)-boost::math::expint(1,Pe*(1+paramMat[1]*Pe)) ) ;
  return jac;
}
struct Funcd3D {
  double operator() (const double x, vector<double> paramMat)
    { return systemCantorVogel3D(x,paramMat); }
  double df(const double x, vector<double> paramMat)
    { return systemCantorVogel3D_Jacobian(x,paramMat); }
};







//===================================================
//Solver of Cantor and Vogel System for 2D Dendrite by Newton Method
//Return Vtip and Rtip in a RectangularMatrix<double>
//===================================================
std::vector<double> CantorVogel2DNewton(double omega, double coeffK, double GibbsThomson,
                  double liq_slope, double Co, double thickness, double Dl,double sigma)
{ //NB: Calcul precision is 1e-16 so it is useless and dangerous to set tolerance inferior to 1e-15
//MZ 2019-11-22: Relaxed convergence conditions.
//  const double  facc=1.0e-12;     //Tolerance on norm(f)
//  const double  Pe_acc=1.0e-12;   //Tolerance on Peclet
  const double  facc=1.0e-6;     //Tolerance on norm(f)
  const double  Pe_acc=1.0e-6;   //Tolerance on Peclet
  const double  Pe_min=1.0e-300;  //Minimum Peclet (NB: boost::math::expint(0)=Inf !!!)
  const double  Pe_max=700;       //Maximum Peclet (NB: exp(7.097827e2)=1.79767e+308)

  //Declaration of Useful Matrix
  std::vector<double> Solution(2);       //Solution of f(X)=0
  Solution[0] = 0;
  Solution[1] = 0;

  //Solve only for non negligible omega
  if(omega>1.e-6 && omega<1-1e-6) {
      vector<double> paramMat(2);
      paramMat[0] = omega;
      paramMat[1] = 4*thickness *sigma*Co*liq_slope*(coeffK-1) / GibbsThomson;
      //Info << "omega=" << omega << endl;
      //Info << "param2=" << paramMat[1] << endl;
      Funcd2D fx;
      //Debug MZ
      double xinit = 0.19*pow(omega/(1-omega), 1.70); // Initial guess from inverse Ivantsov
      //Info << "omega,xinit=" << omega << "  "<< xinit << endl;
      //double Pe = RiddersRoot<Func2D>(Pe_min, Pe_max);
      double Pe=rtsafe(fx,paramMat,xinit,Pe_min,Pe_max,Pe_acc,facc);
      double Rtip=2*thickness/(paramMat[1]*Pe);
      double Vtip=Pe*2*Dl/Rtip;
      Solution[0] = Vtip;
      Solution[1] = Rtip;
    }

  return Solution;
}


//===================================================
//Solver of Cantor and Vogel System for 3D Dendrite by Newton Method
//Return Vtip and Rtip in a RectangularMatrix<double>
//===================================================
std::vector<double> CantorVogel3DNewton(double omega, double coeffK, double GibbsThomson,
                  double liq_slope, double Co, double thickness, double Dl,double sigma)
{ //NB: Calcul precision is 1e-16 so it is useless and dangerous to set tolerance inferior to 1e-15
//MZ 2019-11-22: Relaxed convergence conditions.
//  const double  facc=1.0e-12;     //Tolerance on norm(f)
//  const double  Pe_acc=1.0e-12;   //Tolerance on Peclet
  const double  facc=1.0e-6;     //Tolerance on norm(f)
  const double  Pe_acc=1.0e-6;   //Tolerance on Peclet
  const double  Pe_min=1.0e-300;  //Minimum Peclet (NB: boost::math::expint(0)=Inf !!!)
  const double  Pe_max=700;       //Maximum Peclet (NB: exp(7.097827e2)=1.79767e+308)

  //Declaration of Useful Matrix
  std::vector<double> Solution(2);       //Solution of f(X)=0
  Solution[0] = 0;
  Solution[1] = 0;

  //Solve only for non negligible omega
  if(omega>1.e-6 && omega<1-1e-6) {
      vector<double> paramMat(2);
      paramMat[0] = omega;
      paramMat[1] = 4*thickness *sigma*Co*liq_slope*(coeffK-1) / GibbsThomson;
      Funcd3D fx;
      //Debug MZ
      double xinit = 0.61*pow(omega/(1-omega), 1.32); // Initial guess from inverse Ivantsov
      //Info << "omega,xinit=" << omega << "  "<< xinit << endl;
      double Pe=rtsafe(fx,paramMat,xinit,Pe_min,Pe_max,Pe_acc,facc);
      double Rtip=2*thickness/(paramMat[1]*Pe);
      double Vtip=Pe*2*Dl/Rtip;
      Solution[0] = Vtip;
      Solution[1] = Rtip;
    }

  return Solution;
}
