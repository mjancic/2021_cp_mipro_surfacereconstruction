 	// Dimensional material parameters (parameters from mesoBench are used)
 	// If changed, they should keep the same order of magnitude
 	const double kp0=0.1;
	const double Gibbs=6.525e-08;
	const double liqSlope=-2.1;
	const double D_liq=1.27e-9;
	const double sigma=0.0132;
	const double C0=1.0;
	