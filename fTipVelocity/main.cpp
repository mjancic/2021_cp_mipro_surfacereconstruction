#include <math.h>
#include <cmath>
#include <vector>
//#include <iostream>
//#include <fstream>
using namespace std;
#include "fTipVelocity.h"


int main()
{
	//*** Case parameter (initial condition)
	//*** This is a constant for each simulation
	const double Omega0=0.05;

	//*** Model parameter of the GEM (Grain Envelope Model)
	//*** This is a constant for each simulation
	const double delta_dimless=1.0;




	// Calculate reference state: Ivantsov solution
	// This is called once at the beginning of each simulation.
  	std::vector<double> IvState(3);
	IvState = IvTip(Omega0);
	const double VIv = IvState[0];
	const double RIv = IvState[1];
	const double PeIv= IvState[2];

	


	//*** Dimensionless concentration at dimensionless distance delta_dimless from the envelope. 
	//*** This is an input from the diffusion field solution
	double Omega_delta;
	
	// Calculate dimensionless tip velocity at the envelope
	double Vtip_dimless = fTipVelocity(Omega_delta, delta_dimless, Omega0, PeIv, VIv);

}