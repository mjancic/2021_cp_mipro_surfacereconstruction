#include <math.h>
#include <cmath>
#include <vector>
using namespace std;
#include "dimParameters.h"
#include "fTipVelocity.h"



//===================================================
//Function calculates tip reference state (Ivantsov tip)
//Omega0: dimensionless concentration (supersaturation) at infinity
//===================================================
std::vector<double> IvTip(double Omega0)
{
	// Calculate dimensional concentration at the envelope (for isothermal growth)
	const double envelopeClint=C0/(1-Omega0*(1-kp0));

	// Call CaVo with large delta (delta -> infty)
    vector<double> Solution(2);
  	Solution = CantorVogel2DNewton
//   Solution = CantorVogel3DNewton
    (
   	    Omega0,			//Omega at infinity
       	kp0,
        Gibbs,
   	    liqSlope,
       	envelopeClint,
        1.e6,  			//must be large enough to mimic (delta/Rtip -> infty)
   	    D_liq,
    	sigma
	);
  	std::vector<double> IvState(3);
    IvState[0] = Solution[0];							// VIv
   	IvState[1] = Solution[1];							// RIv
	IvState[2] = Solution[0]*Solution[1]/(2.0*D_liq);	// PeIv

	return IvState;
}




//===================================================
//Function calculates the dimensionless tip velocity at the envelope
//Omega_delta: dimensionless concentration (supersaturation) at distance delta_dimless from the envelope
//delta_dimless: dimensionless distance from the envelope
//PeIv: Reference Peclet number (Peclet of the Ivantsov tip)
//VIv: Reference velocity (velocity of the Ivantsov tip)
//===================================================
double fTipVelocity(double Omega_delta, double delta_dimless, double Omega0, double PeIv, double VIv)
{
	// Scale delta to dimensional
	const double delta = delta_dimless*(D_liq/VIv);

	// Calculate dimensional concentration at the envelope (for isothermal growth)
	const double envelopeClint=C0/(1-Omega0*(1-kp0));


	// Calculate dimensional tip velocity at the envelope (Solution[0])
    vector<double> Solution(2);
   	Solution = CantorVogel2DNewton
//   Solution = CantorVogel3DNewton
    (
   	    Omega_delta,
       	kp0,
        Gibbs,
   	    liqSlope,
       	envelopeClint,
        delta,
   	    D_liq,
    	sigma
	);
    double Petip=Solution[0]*Solution[1]/(2.0*D_liq); //Rtip*Vtip/(2.0*D_liq)

	// Calculate dimensionless tip velocity at the envelope
	double Vtip_dimless = pow(Petip/PeIv,2.0);
	
	return Vtip_dimless;
}








