# Sharp-interface model of dendrite envelope growth

An isothermal interface (_envelope_), which is maintained at a constant dimensionless concentration $`\Omega_l=0`$, grows into an infinite medium (_liquid_) that is initially at a uniform concentration $`\Omega_0`$. In the liquid the mass transfer is governed by the diffusion equation:
```math
    \frac{\partial \Omega_l}{\partial \tilde{t}} = \tilde{\nabla}^2 \Omega_l .
```
Note that all equations in this document are given in a dimensionless formulaiton and that a tilde ($`\tilde{ }`$) designates a dimensionless quantity (with the exception of the conentration, denoted by $`\Omega`$).

![image][scheme]

The envelope represents an envelope of a dendrite whose branches grow
into predefined _tip[^footnote-1] growth directions_. 
A branch of a real dendrite is shown in the figure below.
[^footnote-1]: Tips of the dendrite branches.

![image][dendriteMelendez]

We can see small branches growing from a primary trunk. These growth directions introduce an anisotropy of growth 
speed for the envelope. In the model the anisotropy is introduced by assuming 
that the (imaginary) tip behind the envelope grows into the tip growth direction 
that forms the smallest angle with the normal, $`\vec{n}`$, of the envelope. 
Let us look at the example shown in the schematics, where 4 tip growth 
directions are indicated for 2D growth. The relation between tip growth direction 
and envelope normal is shown for two locations on the envelope. In one case the 
angle of $`\vec{n}`$ with $`\theta_2`$ is smaller and the tip velocity, 
$`\vec{v}_\mathrm{tip}`$, is oriented into direction 2 (vertically upwards). 
In the second case the angle with $`\theta_1`$ is smaller and the tip is assumed 
to grow into direction 1 (horizontally to the left). The growth velocity of the 
envelope in the normal direction is then calculated from the projection of the 
selected tip velocity onto the normal:
```math
	\tilde{v}_n = \tilde{v}_\mathrm{tip} \cos \left( \min(\theta_i) \right) \,,
```
where $`\theta_i`$ are the tip growth directions.

The tip velocity is a function of the concentration field in the vicinity of the 
envelope. Precisely, it is given by an analytically formulated function of the 
difference between the stagnant-film concentration, $`\Omega_\delta`$ and the envelope concentration, $`\Omega_l=0`$, and of the stagnant-film thickness, $`\tilde{\delta}`$.
```math
	\tilde{v}_\mathrm{tip} = f\left( \Omega_\delta, \tilde{\delta} \right)
```
The function $`f`$ is defined by the following system of equations:
```math
	Pe_\mathrm{tip} = \mathrm{CaVo}^{-1} \left( \Omega_\delta, \frac{\tilde{\delta}}{\tilde{R}_\mathrm{tip}} \right)
```
```math
	\tilde{R}_\mathrm{tip} = \frac{ 2 Pe_\mathrm{Iv}^2}{Pe_\mathrm{tip}}
```
```math
	\tilde{v}_\mathrm{tip} = \left( \frac{Pe_\mathrm{tip}}{Pe_\mathrm{Iv}} \right)^2
```
$`x=\mathrm{CaVo}^{-1}(y,A)`$ is the inverse of the so-called Cantor&Vogel function. In 2D:
```math
	y=\mathrm{CaVo}(x,A) = \sqrt{\pi x} \exp(x) \left[ \mathrm{erfc}(\sqrt{x})- \mathrm{erfc}( \sqrt{x [1+2A]} ) \right].
```

$`Pe_\mathrm{Iv}`$ is a constant reference scale that is a function only of the initial far-field concentration $`\Omega_0`$:
```math
	Pe_\mathrm{Iv} = \mathrm{Iv}^{-1} (\Omega_0),
```
where $`\mathrm{Iv}^{-1}`$ is the inverse of the so-called Ivantsov function, $`\mathrm{Iv}`$:
```math
	\mathrm{Iv}(x) = \mathrm{CaVo} \left( x, \frac{\tilde{\delta}}{\tilde{R}_\mathrm{tip}} \rightarrow \infty \right)
```


The stagnant-film concentration, $`\Omega_\delta`$, is defined as the concentration at distance $`\tilde{\delta} \cdot \vec{\tilde{n}}`$ from the envelope, where $`\vec{\tilde{n}}`$ is the outward pointing normal of the envelope surface.

Typically, $`\tilde{\delta}`$ is larger than the radius of curvature of the envelope at the tip (where the radius of curvature is the smallest). $`\tilde{\delta}`$ must be identical all over the envelope. Therefore an accurate determination of the distance $`\tilde{\delta} \cdot \vec{\tilde{n}}`$ and an accurate interpolation of the temperature $`\Omega_\delta`$ are required.

The figures below show an example of the solution. We can see that the whole grain grows continuously and that its shape evolves. 

![image][grainFullTfield]
![image][grainFull0]
![image][grainFull1]
![image][grainFull2]
![image][grainFull3]
![image][grainFull4]
![image][grainFull5]

However, the part of the envelope close to the tip reaches a steady state after a certain time, shown below. 

![image][grainTipTfield]

In steady state the growth speed of the tip of the envelope is constant and the 
temperature field of the tip region is steady in the reference frame of the tip. 
We are interested in the shape of the envelope and in the temperature field around 
the envelope in _steady-state_. All results shown here were obtained with a model 
that uses a diffuse-interface interface capturing algorithm to describe the envelope 
and does not impose the temperature boundary condition on the interface in an exact manner. 
A more precise method is sought to validate the description of the envelope, which is 
particularly important in cases where the envelope destabilizes and new branchlike structures are created.


[scheme]: figs/mesoScheme_v2.png "Schematics of the problem."
[dendriteMelendez]: figs/dendrite-Melendez2012.jpg "A real dendrite."
[grainFullTfield]: figs/Full_tOvertS_30.png "Full grain and the temperature field."
[grainFull0]: figs/0p05_00000.png "Envelope growth for two grains growing towards each other."
[grainFull1]: figs/0p05_01000.png "Envelope growth for two grains growing towards each other."
[grainFull2]: figs/0p05_05000.png "Envelope growth for two grains growing towards each other."
[grainFull3]: figs/0p05_15000.png "Envelope growth for two grains growing towards each other."
[grainFull4]: figs/0p05_25000.png "Envelope growth for two grains growing towards each other."
[grainFull5]: figs/0p05_83000.png "Envelope growth for two grains growing towards each other."
[grainTipTfield]: figs/env_Omega_010_b.png "Steady state in the tip region."

