#ifndef SPLINESHAPE_SPLINESHAPE_HPP
#define SPLINESHAPE_SPLINESHAPE_HPP

#include <../../eigen/unsupported/Eigen/Splines>
#include <medusa/Medusa.hpp>

using namespace mm;

template <class T>
bool matlab_dump(const T& range, const std::string& filename, const std::string& var_name) {
    const std::string dir = "../data/";
    std::string path = dir + filename;
    std::ofstream out_file(path);
    out_file << var_name << " = " << range << ";" << std::endl;
    out_file.close();
    return true;
}

template <typename vec_t>
class SplineShape : public DomainShape<vec_t> {
    // A shape used to reconstruct the edge of discretely given domain using Catmull-Rom splines.
    // Also contains a method to determine the interior/exterior of the shape based on the spline.
  public:
    typedef vec_t vector_t;
    typedef typename vec_t::scalar_t scalar_t;
    typedef Eigen::Vector<scalar_t, 1> domain_t;
    enum { dim = vec_t::dim };

    SplineShape(const Range<vector_t>& pts, int seed = 1, bool order = true, int iter_reps = 10) {
        init(pts, iter_reps, seed, order);
    }

    SplineShape(const Range<vector_t> pts, int ir, int s, const Range<int> p, const Range<int> pi,
                Range<vector_t> nrmls, int nc) {
        static_assert(vector_t::dim == 2, "SplineShape only supports 2D vectors");
        unordered_points = pts;
        iter_reps = ir;
        seed = s;
        number_of_points = pts.size();
        // interior_corr = ic;
        permutation = p;
        inv_permutation = pi;
        kdtree.reset(unordered_points);
        normals = nrmls;
        normal_corr = nc;

        points = [this](int n) -> vector_t {
            return unordered_points[permutation[q(n, number_of_points)]];
        };

        Eigen::Array<scalar_t, 2, Eigen::Dynamic> ordered_points =
            Eigen::Array<scalar_t, 2, Eigen::Dynamic>::Zero(2, number_of_points + 3);
        for (int i = 0; i < number_of_points + 3; i++) {
            ordered_points.col(i) = points(i).transpose();
        }
        Eigen::ChordLengths(ordered_points, knots);
        spline = Eigen::SplineFitting<Eigen::Spline<scalar_t, 2, 3>>::Interpolate(ordered_points, 3,
                                                                                  knots);
    }

    // To decide if a point is contained in the shape, find the nearest point on the spline and
    // observe the direction of the normal at that point.
    bool contains(const vector_t& point) const override {
        // scalar_t s = nearestSplinePoint(point);
        // int s0 = q(std::round(s), number_of_points);
        scalar_t s;
        int s0;
        std::tie(s, s0) = nearestSplinePoint(point);
        vector_t spline_point = evalSpline(s);
        vector_t normal = evalTangent(s);
        normal = vector_t(-normal(1), normal(0));
        normal *= sgn(normal.dot(normal_corr * normals[s0]));
        scalar_t prod = (spline_point - point).dot(normal);
        // return prod * interior_corr >= 0;
        return prod <= 0;
    }

    bool hasContains() const override { return true; };

    // Since determining the interior of a general shape is hard, the algorithm needs
    // to be 'tuned' to know what's the interior. This can be done by calling setInterior
    // with a known interior/exterior point and setting interior_point
    // to true/false resp. This simpy sets 'interior_corr' to +1 or -1 as appropriate.
    void setInterior(const vector_t& v, bool interior_point = true) {
        bool point_contained = contains(v);
        // if (point_contained != interior_point) {
        //    interior_corr *= -1;
        //}
        // bool normals_pointing_inwards = contains(points(0) + normals[0]);
        // if (normals_pointing_inwards) {
        //    normal_corr *= -1;
        //}
        if (point_contained != interior_point) {
            normal_corr *= -1;
        }
    }

    std::pair<vector_t, vector_t> bbox() const override {
        // First we find the extreme points of the starting set of points
        // with regard to the x and y coordinates.
        int extreme_inds[4] = {0, 0, 0, 0};  // {min_x_ind, max_x_ind, min_y_ind, max_y_ind}
        // extreme_vals ... {min_x, max_x, min_y, max_y}
        scalar_t extreme_vals[4] = {points(0)(0), points(0)(0), points(0)(1), points(0)(1)};
        for (int i = 0; i < number_of_points; ++i) {
            if (points(i)(0) < extreme_vals[0]) {
                extreme_vals[0] = points(i)(0);
                extreme_inds[0] = i;
            }
            if (points(i)(0) > extreme_vals[1]) {
                extreme_vals[1] = points(i)(0);
                extreme_inds[1] = i;
            }
            if (points(i)(1) < extreme_vals[2]) {
                extreme_vals[2] = points(i)(1);
                extreme_inds[2] = i;
            }
            if (points(i)(1) > extreme_vals[3]) {
                extreme_vals[3] = points(i)(1);
                extreme_inds[3] = i;
            }
        }
        // Then we find the nearest neighbour of each extreme point.
        Range<scalar_t> dists;
        Range<int> inds;
        scalar_t nb_dists[4];
        for (int direction = 0; direction < 4; ++direction) {
            std::tie(inds, dists) = kdtree.query(points(extreme_inds[direction]), 2);
            nb_dists[direction] = dists[1];
        }
        // Assuming the discretization is dense enough, the shape should be contained
        // in a rectangle at most (nb_dists)-away from the starting points.
        vector_t mins(extreme_vals[0] - 2 * nb_dists[0], extreme_vals[2] - 2 * nb_dists[2]);
        vector_t maxs(extreme_vals[1] + 2 * nb_dists[1], extreme_vals[3] + 2 * nb_dists[3]);
        return std::pair<vector_t, vector_t>(mins, maxs);
    }

    SplineShape<vector_t>* clone() const override {
        return new SplineShape<vector_t>(unordered_points, iter_reps, seed, permutation,
                                         inv_permutation, normals, normal_corr);
    }

    std::ostream& print(std::ostream& os) const override {
        return os << "SplineShape(" << vector_t::dim << ")";
    }

    DomainDiscretization<vector_t> discretizeBoundaryWithDensity(
        const std::function<scalar_t(vector_t)>& dr, int type) const override {
        // We intend to fill a DomainDiscretization object using GSF.
        DomainDiscretization<vector_t> domain(*this);
        // Since we 'glued together' the domains of all the spline parts, we only need to call gsf
        // once. First create a new gsf object and set the seed.
        GeneralSurfaceFill<vector_t, domain_t> gsf;
        gsf.seed(seed);
        // Now create a wrapper for the spline evaluation functions so that they can be given to
        // gsf.
        auto r = [this](domain_t v) { return this->evalSpline(v(0)); };
        auto Dr = [this](domain_t v) { return this->evalTangent(v(0)); };
        // Create an object representing the domain of the spline.
        BoxShape<domain_t> shape =
            BoxShape<domain_t>(domain_t(0), domain_t(knots(knots.cols() - 3)));
        DomainDiscretization<domain_t> param_dom(shape);
        // Finally, call gsf.
        gsf(domain, param_dom, r, Dr, dr, type);
        // GSF generates its own normals - theese might need fixing,
        // so we replace them with our normals.
        for (int i = 0; i < domain.size(); i++) {
            domain.normal(i) = getNormal(param_dom.pos(i)(0));
        }
        return domain;
    }

    // An override to 'discretizeBoundaryWithDensity' allowing it to be called with a constant
    // spacing.
    DomainDiscretization<vector_t> discretizeBoundaryWithDensity(const scalar_t h, int type) const {
        std::function<scalar_t(vector_t)> spacing = [h](vector_t v) { return h; };
        return discretizeBoundaryWithDensity(spacing, type);
    }

  private:
    // The original Range of points given to an object. We assume it is not ordered.
    Range<vector_t> unordered_points;
    // The seed given to gsf.
    int seed;
    // The size of 'unordered_points'
    int number_of_points;
    // A +1/-1 factor used to 'tune' interior/exterior.
    // int interior_corr;
    // A permutation that reorders 'unordered_points' so that successive points are next to each
    // other on the curve.
    Range<int> permutation;
    // The inverse of the above permutation. We use this to get the correct index of a point and so
    // that we don't need to rebuild the kd-tree when we reorder the points. I.e. to find the
    // nearest starting point of a generic point we use the 'old' kd-tree build on the unordered
    // points and use this to get its 'ordered' index.
    Range<int> inv_permutation;
    // A kd-tree used in finding the k-nearest neighbors of a point. This is based on
    // 'unordered_points'.
    KDTree<vector_t> kdtree;
    // A function that applies both the permutation 'permutation' and the function 'q' to the
    // unordered points to get the appropriate cyclic ordering.
    std::function<vector_t(int)> points;
    // A range containing the normals at each point (using ordered indexing).
    Range<vector_t> normals;
    // A +1/-1 factor used to make sure the normals point outwards
    int normal_corr;
    // The number of steps of bisection to use in approximating the nearest point
    int iter_reps;
    // An array of knots for the spline
    Eigen::Array<scalar_t, 1, Eigen::Dynamic> knots;
    // The spline object
    Eigen::Spline<scalar_t, 2, 3> spline;

    void init(const Range<vector_t>& pts, int k, int s, bool order) {
        static_assert(vector_t::dim == 2, "SplineShape only supports 2D vectors");
        unordered_points = pts;
        iter_reps = k;
        seed = s;
        number_of_points = pts.size();
        // interior_corr = 1;
        permutation = Range<int>(number_of_points);
        inv_permutation = Range<int>(number_of_points);
        kdtree.reset(unordered_points);
        normal_corr = 1;

        Range<scalar_t> dists;
        Range<int> inds;

        if (order) {
            // Order points so that the point indexed by order_permutation[i+1] is a neighbor of
            // order_permutation[i]. First set up necessary variables.
            permutation[0] = 0;
            inv_permutation[0] = 0;

            // Starting at the 0-th point of 'unordered_points', find the next point.
            for (int i = 0; i + 1 < number_of_points; ++i) {
                // Start by looking at the nn = 2 knn, if none are applicable, increase nn by 1.
                int nn = 1;
                bool looking_for_nbr = true;
                int candidate;
                while (looking_for_nbr) {
                    nn++;  // Increase nn by 1 - every time we don't find the right index.
                    // Find the nn knn of the current point, check each if it wasn't already
                    // enumerated.
                    std::tie(inds, dists) = kdtree.query(unordered_points[permutation[i]], nn);
                    // Look at the latest candidate - the last one returned by query - since we
                    // checked all others and the 0th one is just the current point.
                    candidate = nn - 1;
                    // Check if the newly found point is a repeat of the current point.
                    assert_msg(dists[candidate] != 0, "Repeated points found.");
                    bool candidate_ok = true;
                    // Compare the index of the current candidate to the previous nn points.
                    // If it was already enumerated, skip.
                    for (int prev = 1; prev < nn; prev++) {
                        candidate_ok = (inds[candidate] == permutation[std::max(0, i - prev)])
                                           ? false
                                           : candidate_ok;
                    }
                    // When we eventually find the next neighbor, set it as the next point.
                    if (candidate_ok) {
                        permutation[i + 1] = inds[candidate];
                        looking_for_nbr = false;
                        inv_permutation[permutation[i + 1]] = i + 1;
                    }
                }
            }
        } else {
            permutation = Range<int>::seq(number_of_points);
            inv_permutation = permutation;
        }

        // Now define 'points' - essentially a wrapper applying 'permutation' and 'q'.
        points = [this](int n) -> vector_t {
            return unordered_points[permutation[q(n, number_of_points)]];
        };

        Eigen::Array<scalar_t, 2, Eigen::Dynamic> ordered_points =
            Eigen::Array<scalar_t, 2, Eigen::Dynamic>::Zero(2, number_of_points + 3);
        for (int i = 0; i < number_of_points + 3; i++) {
            ordered_points.col(i) = points(i).transpose();
        }

        Eigen::ChordLengths(ordered_points, knots);
        spline = Eigen::SplineFitting<Eigen::Spline<scalar_t, 2, 3>>::Interpolate(ordered_points, 3,
                                                                                  knots);

        // Now we calculate an approximation for the normals at each starting point based on the
        // spline.
        normals = Range<vector_t>(number_of_points);
        // We also save the lengths of the normals, since we will need them to fit evalRBF.
        Range<scalar_t> normals_norms(number_of_points);
        Eigen::Matrix2d rot_mat;
        rot_mat << 0, -1, 1, 0;
        for (int i = 0; i < number_of_points; ++i) {
            // To get a normal, simply rotate the tangent at each point by pi/2.
            normals[i] = rot_mat * evalTangent(knots[i]);
            // Scale the normals so that they do not 'intersect' the shape, i.e. so that the line
            // segment [point(i), point(i) + normals[i]] does not intersect the edge of the shape.
            // We use a kludge relying on the fact that the starting points need to be dense enough
            // to determine all the contours of the shape.
            std::tie(inds, dists) = kdtree.query(points(i), 2);
            normals_norms[i] = dists[1];
            normals[i] = normals[i] / normals[i].norm() * normals_norms[i] * 0.9;
            // Finally propagate correct orientation of normals. Since we have no guarantee that the
            // normals are coherently oriented, we make sure that each normal 'points in the same
            // direction' as the previous one. This again requires that the starting points are
            // dense enough.
            if (i > 0) {
                normals[i] *= sgn(normals[i].dot(normals[i - 1]));
            }
        }
    }

    // Finds a scalar_t s that minimizes the distance between evalSpline(-) and query_pt
    std::tuple<scalar_t, int> nearestSplinePoint(const vector_t& query_pt) const {
        Range<int> inds;
        Range<scalar_t> dists;
        std::tie(inds, dists) = kdtree.query(query_pt, 1);
        int nearest_pt = inv_permutation[inds[0]];
        std::function<scalar_t(scalar_t)> sq_distance = [&query_pt, this](scalar_t t) -> scalar_t {
            return (evalSpline(wrap(t)) - query_pt).squaredNorm();
        };
        std::function<scalar_t(scalar_t)> d_sq_distance = [&query_pt,
                                                           this](scalar_t t) -> scalar_t {
            return 2 * (evalSpline(wrap(t)) - query_pt).dot(evalTangent(wrap(t)));
        };
        scalar_t lower = knots(q(nearest_pt - 1, number_of_points)),
                 upper = knots(q(nearest_pt + 1, number_of_points));
        scalar_t s = knots(nearest_pt);
        for (int i = 0; i < iter_reps; ++i) {
            if (sgn(d_sq_distance(lower) * d_sq_distance(wrap(s))) <= 0) {
                upper = wrap(s);
            } else if (sgn(d_sq_distance(wrap(s)) * d_sq_distance(upper)) <= 0) {
                lower = wrap(s);
            }
            s = wrap(wrap(upper - lower) / 2 + lower);
        }
        return std::make_tuple(s, nearest_pt);
    }

    vector_t evalSpline(scalar_t u) const { return Vec2d(spline(u)); }

    vector_t evalTangent(scalar_t u) const { return Vec2d(spline.derivatives(u, 1).col(1)); }

    vector_t getNormal(scalar_t u) const {
        Range<int> inds;
        Range<scalar_t> dists;
        std::tie(inds, dists) = kdtree.query(evalSpline(u), 1);
        int nearest_pt = inv_permutation[inds[0]];
        vector_t normal = evalTangent(u);
        normal = vector_t(-normal(1), normal(0));
        normal *= sgn(normal.dot(normal_corr * normals[nearest_pt]));
        return normal;
    }

    // The quotient map Z -> Z/nZ
    static int q(int m, int n) { return (m % n >= 0) ? m % n : m % n + n; }

    // The quotient map R -> S^1
    scalar_t wrap(scalar_t t) const {
        scalar_t end = knots(knots.cols() - 3);
        scalar_t rem = std::fmod(t, end);
        return (rem >= 0) ? rem : rem + end;
    }

    // The sign function (except that sgn(0) == 1)
    static int sgn(scalar_t x) { return 2 * (x >= 0) - 1; }
};

#endif  // SPLINESHAPE_SPLINESHAPE_HPP
