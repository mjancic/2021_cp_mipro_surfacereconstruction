#ifndef DENDRITE_GROWTH_HELPER_H
#define DENDRITE_GROWTH_HELPER_H

using namespace std;
using namespace mm;
using json = nlohmann::json;

template <typename vec_t>
double dx(const vec_t& p, KDTree<vec_t>& tree, const json& conf) {
    Range<double> distances2;
    Range<int> closest;
    double domain_radius = conf["liquid"]["domain"]["radius"];
    double dx_dendrite = conf["dendrite"]["dx"];
    double dx_liquid = conf["liquid"]["dx"];

    // Get 2 points closest to p.
    std::tie(closest, distances2) = tree.query(p, 2);
    vec_t p_dendrite = tree.get(closest[1]);  // Coordinates of the closest point.

    // Find point on liquid boundary in the direction of p_dendrite -> p.
    double a = (p - p_dendrite).dot(p - p_dendrite);
    double b = 2 * (p - p_dendrite).dot(p_dendrite);
    double c = p_dendrite.dot(p_dendrite) - domain_radius * domain_radius;
    double t1 = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
    double t2 = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);
    double t = max(t1, t2);

    vec_t p_liquid = p_dendrite + t * (p - p_dendrite);

    return dx_dendrite +
           (dx_liquid - dx_dendrite) * (p - p_dendrite).norm() / (p_liquid - p_dendrite).norm();
}

#endif
