#include <medusa/Medusa.hpp>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseCore>

using namespace mm;

int main() {
    // Create the domain and discretize it.
    BallShape<Vec2d> ball({0, 0}, 1.0);
    BoxShape<Vec2d> box({0, 0}, {2.0, 2.0});
    double dx = 0.05;
    DomainDiscretization<Vec2d> domain = ball.discretizeBoundaryWithStep(dx);
    DomainDiscretization<Vec2d> d = box.discretizeBoundaryWithStep(dx);
    domain -= d;
    // Fill engine.
    GeneralFill<Vec2d> fill_randomized;
    fill_randomized.seed(235);
    // Fill domain with nodes.
    domain.fill(fill_randomized, dx);

    // Find support for the nodes.
    int N = domain.size();
    domain.findSupport(FindClosest(15));

    // Construct the approximation engine.
    int m = 2;  // Augmentation.
    Polyharmonic<double, 3> ph;
    RBFFD<decltype(ph), Vec2d, ScaleToFarthest> appr(ph, Monomials<Vec2d>(m));

    auto storage = domain.computeShapes<sh::lap>(appr);

    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    Eigen::VectorXd rhs(N);
    rhs.setZero();
    M.reserve(storage.supportSizes());

    // Construct implicit operators over our storage
    auto op = storage.implicitOperators(M, rhs);

    for (int i : domain.interior()) {
        double x = domain.pos(i, 0);
        double y = domain.pos(i, 1);
        // set the case for nodes in the domain
        op.lap(i) = -2 * PI * PI * std::sin(PI * x) * std::sin(PI * y);
    }
    for (int i : domain.boundary()) {
        // Enforce the boundary conditions
        op.value(i) = 0.0;
    }

    Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
    solver.compute(M);
    ScalarFieldd u = solver.solve(rhs);

    // Write the solution into file
    // Create H5 file to store the solution.
    const std::string output_file = "../data/results_poisson.h5";
    HDF file(output_file, HDF::DESTROY);
    file.openGroup("/");
    file.writeDomain("domain", domain);
    file.writeFloatArray("sol", u);
    // Close file.
    file.close();

    return 0;
}
