#ifndef PHASE_CHANGE_SCATTERED_INTERPOLANT_H
#define PHASE_CHANGE_SCATTERED_INTERPOLANT_H

using namespace std;
using namespace mm;

/**
 * Scattered node interpolant averaging over small neighbourhood.
 * Implements modified Sheppard's method.
 */
template <class vec_t, class value_t>
class ScatteredInterpolant {
    KDTree<vec_t> tree;  ///< Tree of all points.
    Range<value_t> values;  ///< Function values at given points.
    int num_closest;  ///< Number of closest points to include in calculations.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar data type.
public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param values Function values at given positions.
     * @param num_closest How many neighbours to take into account when calculating function value.
     */
    ScatteredInterpolant(const Range<vec_t>& pos, int num_closest)
            : tree(pos), num_closest(num_closest) {}

    ScatteredInterpolant(int num_closest)
            : num_closest(num_closest) {}

    template <typename values_t>
    void setValues(const values_t& new_values) { values = Range<value_t>(new_values.begin(), new_values.end()); }

    void setPositions(const Range<vec_t>& pos) { tree.reset(pos); }

    /// Evaluate the distribution function.
    value_t operator()(const vec_t& point) const {
        auto [idx, d2] = tree.query(point, num_closest);
        if (d2[0] < 1e-6) return values[idx[0]];
        int n = idx.size();
        scalar_t s = 0.0, r = 0.0;
        Range<scalar_t> id2(n);
        for (int i = 0; i < n; ++i) { id2[i] = 1.0/d2[i]; s += id2[i]; }
        for (int i = 0; i < n; ++i) { id2[i] /= s; }
        for (int i = 0; i < n; ++i) { r += id2[i]*values[idx[i]]; }
        return r;
    }
};

#endif //PHASE_CHANGE_SCATTERED_INTERPOLANT_H
