#ifndef DENDRITE_GROWTH_CASE_H
#define DENDRITE_GROWTH_CASE_H

#include "../include/splineshape/SplineShape.hpp"
#include "scattered_interpolant.h"

using namespace std;
using namespace mm;
using json = nlohmann::json;

template <typename vec_t>
struct DendriteGrowthCase {
  public:
    /**
     * @brief Solve explicit time loop.
     *
     * @param conf Configuration file.
     * @param hdf Output file.
     * @param d Domain.
     * @param timer Timer.
     */
    static void solve(const json& conf, HDF& hdf, DomainDiscretization<vec_t>& d, Timer& timer) {
        double dt = conf["simulation"]["dt"];
        double time_steps = conf["simulation"]["time_steps"];

        ScalarFieldd u(d.size());
        auto d_old = d;
        for (int time_step = 0; time_step <= time_steps; time_step++) {
            // Compute simulation current time.
            double time = time_step * dt;

            // Solve problem.
            if (time_step == 0) {
                set_intial_conditions(u, conf, d);
            } else {
                u = solve_problem(conf, u, d, d_old);
            }

            // Save to file.
            if (time_step % (int)conf["meta"]["save_every"] == 0) {
                prn(time);
                prn("Saving to file.");

                hdf.setGroupName(format("step_%06d", time_step));
                hdf.atomic().writeDomain("domain", d);
                hdf.atomic().writeDoubleArray("solution", u);
                hdf.atomic().writeDoubleAttribute("time", time);
            }

            // Domain growth.
            d_old = d;
            d = apply_domain_growth(conf, d);
        }
    };

  private:
    /**
     * @brief Consider domain growth.
     *
     * @param conf Configuration file.
     * @param d Domain.
     * @return DomainDiscretization<vec_t> New domain.
     */
    static DomainDiscretization<vec_t> apply_domain_growth(const json& conf,
                                                           DomainDiscretization<vec_t>& d) {
        // Move dendrite nodes.
        auto new_dendrite_nodes = get_new_dendrite_boundary(conf, d);

        // Build dendrite shape from new nodes.
        SplineShape dendrite_shape(new_dendrite_nodes);
        vec_t centre(vec_t::dim);
        centre.setZero();
        dendrite_shape.setInterior(centre);
        // Discretize boundary.
        double h = conf["dendrite"]["dx"];
        int type = conf["dendrite"]["domain"]["node_type"];
        DomainDiscretization<vec_t> d_dendrite =
            dendrite_shape.discretizeBoundaryWithDensity(h, type);
        auto dendrite_boundary_nodes = d_dendrite.positions();

        // Build liquid shape.
        double origin = conf["liquid"]["domain"]["origin"];
        double radius = conf["liquid"]["domain"]["radius"];
        BallShape<vec_t> liquid_shape(origin, radius);
        // Discretize boundary.
        h = conf["liquid"]["dx"];
        type = conf["liquid"]["domain"]["node_type"];
        DomainDiscretization<vec_t> domain = liquid_shape.discretizeBoundaryWithStep(h, type);

        // Fill liquid domain.
        domain -= d_dendrite;
        KDTree<vec_t> tree(dendrite_boundary_nodes);
        auto fn = [&](const vec_t& p) { return dx(p, tree, conf); };
        // Fill engine.
        GeneralFill<vec_t> fill_randomized;
        fill_randomized.numSamples(conf["domain"]["fill"]["num_samples"])
            .seed(conf["domain"]["fill"]["seed"]);
        // Fill domain with nodes.
        domain.fill(fill_randomized, fn);

        return domain;
    }

    /**
     * @brief Get the new dendrite boundary.
     *
     * @param conf Configuration file.
     * @param d Domain.
     * @return Range<vec_t> New dendrite boundary node positions.
     */
    static Range<vec_t> get_new_dendrite_boundary(const json& conf,
                                                  const DomainDiscretization<vec_t>& d) {
        // Get current dendrite nodes.
        int type = conf["dendrite"]["domain"]["node_type"];
        auto dendrite_boundary_idx = d.types() == type;
        Range<vec_t> dendrite_nodes = d.positions()[dendrite_boundary_idx];
        Range<vec_t> dendrite_normals = d.normals()[dendrite_boundary_idx];

        // Reposition nodes.
        double velocity = conf["temp"]["velocity"];
        double time_step = conf["simulation"]["dt"];
        for (int i = 0; i < dendrite_nodes.size(); i++) {
            auto pos = dendrite_nodes[i];
            auto normal = dendrite_normals[i];

            // For circular expansion.
            // vec_t moved_node = pos * (1 + velocity);
            // dendrite_nodes[i] = moved_node;

            // For star-like expansion.
            double phi = std::atan2(pos(1), pos(0));
            double v = mm::ipow(std::cos(2 * phi), 2) + 0.05;
            v *= velocity;
            dendrite_nodes[i] = pos +  time_step * v * normal;

            // For circular expansion using domain normals.
            // vec_t moved_node = pos + velocity * * normal;
            // dendrite_nodes[i] = moved_node;
        }

        return dendrite_nodes;
    }

    static ScalarFieldd solve_problem(const json& conf, const ScalarFieldd& u_old,
                                      DomainDiscretization<vec_t>& d,
                                      const DomainDiscretization<vec_t>& d_old) {
        // Map scalar field values to new domain positions.
        ScalarFieldd u = map_solution_to_new_domain(conf, u_old, d, d_old);

        // Find support.
        d.findSupport(FindClosest((int)conf["approximation"]["support_size"]));
        // Compute shapes. Only Laplacian operator needed.
        Polyharmonic<double, 3> ph;
        RBFFD<decltype(ph), vec_t, ScaleToFarthest> appr(
            ph, Monomials<vec_t>((int)conf["approximation"]["augmentation_degree"]));
        auto storage = d.template computeShapes<sh::lap>(appr);
        auto op = storage.explicitOperators();

        // Problem.
        auto interior = d.interior();
        double dt = conf["simulation"]["dt"];
        double lambda = (double)conf["liquid"]["properties"]["k"] /
                        ((double)conf["liquid"]["properties"]["rho"] *
                         (double)conf["liquid"]["properties"]["cp"]);
        for (int i : interior) {
            u[i] = u[i] + dt * lambda * op.lap(u, i);
        }

        // Return solution.
        return u;
    }

    /**
     * @brief Set the intial conditions.
     *
     * @param u Scalar field.
     * @param conf Configuration params.
     * @param d Domain.
     */
    static void set_intial_conditions(ScalarFieldd& u, const json& conf,
                                      const DomainDiscretization<vec_t> d) {
        u.setOnes();

        // Liquid.
        u = u * (double)conf["liquid"]["temperature"];

        // Dendrite and liquid boundary.
        set_boundary_values(u, conf, d);
    }

    /**
     * @brief Set the boundary values.
     *
     * @param u Scalar field.
     * @param conf Configuration params.
     * @param d Domain.
     */
    static void set_boundary_values(ScalarFieldd& u, const json& conf,
                                    const DomainDiscretization<vec_t> d) {
        // Dendrite.
        int type = conf["dendrite"]["domain"]["node_type"];
        auto boundary_idx = d.types() == type;
        for (int i : boundary_idx) {
            u[i] = (double)conf["dendrite"]["temperature"];
        }

        // Liquid.
        type = conf["liquid"]["domain"]["node_type"];
        boundary_idx = d.types() == type;
        for (int i : boundary_idx) {
            u[i] = (double)conf["liquid"]["temperature"];
        }
    }

    /**
     * @brief Maps scalafield to new domain positions using Sheppard.
     *
     * @param conf Configuration parameters.
     * @param u_old Current scalar field.
     * @param d New domain.
     * @param d_old Current domain.
     * @return ScalarFieldd New scalar field.
     */
    static ScalarFieldd map_solution_to_new_domain(const json& conf, const ScalarFieldd& u_old,
                                                   const DomainDiscretization<vec_t>& d,
                                                   const DomainDiscretization<vec_t>& d_old) {
        // Setup interpolant.
        ScatteredInterpolant<vec_t, double> interpolant(
            (int)conf["field_interpolation"]["closest"]);
        interpolant.setPositions(d_old.positions());
        interpolant.setValues(u_old);

        // Map field values to new domain positions.
        ScalarFieldd u(d.size());
        Range<int> interior = d.interior();
        for (int i : interior) {
            u[i] = interpolant(d.pos(i));
        }

        // Re-set boundary values.
        set_boundary_values(u, conf, d);

        return u;
    }
};

#endif
