#include <medusa/Medusa.hpp>
#include <nlohmann/json.hpp>

#include "helper.hpp"
#include "dendrite_growth_case.hpp"

// *************************************************************************************
// Prototype trying to simulate mesh evolution on dendrite boundary
//
// One run parameter expected: input xml
// Example: ./proto ../input/settings_proto.json
// *************************************************************************************

using namespace mm;
using namespace std;
using json = nlohmann::json;

template <typename vec_t>
void run_dendrite_growth(const json& conf, HDF& hdf) {
    // Begin.
    Timer t;
    t.addCheckPoint("begin");

    // Create domains.
    // Dendrite.
    double origin = conf["dendrite"]["domain"]["origin"];
    double radius = conf["dendrite"]["domain"]["radius"];
    BallShape<vec_t> dendrite(origin, radius);
    int type = conf["dendrite"]["domain"]["node_type"];
    double h = conf["dendrite"]["dx"];
    DomainDiscretization<vec_t> d_dendrite = dendrite.discretizeBoundaryWithStep(h, type);
    // Liquid.
    origin = conf["liquid"]["domain"]["origin"];
    radius = conf["liquid"]["domain"]["radius"];
    BallShape<vec_t> liquid(origin, radius);
    h = conf["liquid"]["dx"];
    type = conf["liquid"]["domain"]["node_type"];
    DomainDiscretization<vec_t> d = liquid.discretizeBoundaryWithStep(h, type);

    // Domain.
    d -= d_dendrite;
    KDTree<vec_t> tree(d_dendrite.positions());
    auto fn = [&](const vec_t& p) { return dx(p, tree, conf); };
    // Fill engine.
    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf["domain"]["fill"]["num_samples"])
        .seed(conf["domain"]["fill"]["seed"]);
    // Fill domain with nodes.
    d.fill(fill_randomized, fn);

    // Solve.
    DendriteGrowthCase<vec_t>::solve(conf, hdf, d, t);

    // End.
    t.addCheckPoint("end");

    // Save to file.
    hdf.reopen();
    hdf.openGroup("/");
    hdf.writeTimer("timer", t);}

int main(int argc, char* argv[]) {
    assert_msg(argc >= 2, "Second argument should be the json parameter file.");
    // Timer.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    std::cout << "Reading params from: " << argv[1] << std::endl;
    json conf;
    // Create and open a text file.
    ifstream input_json(argv[1]);
    // Convert to json object.
    conf = json::parse(input_json);

    // Create H5 file to store the solution.
    const string output_file = conf["meta"]["out_file"];
    HDF file(output_file, HDF::DESTROY);
    // Write json params to results file.
    file.writeStringAttribute("conf", conf.dump());
    // Close file.
    file.close();

    // Run solution procedure.
    const int dim = conf["domain"]["dim"];
    switch (dim) {
        case 2:
            run_dendrite_growth<Vec<double, 2>>(conf, file);
            break;
        default:
            cout << "Domain dimension (dim = " << dim << ") not expected." << endl;
    };

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_file << "." << endl;
    return 0;
}
