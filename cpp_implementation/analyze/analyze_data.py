"""
    Opens the H5 file and exports relevant data to json files.
    Run example:

    python3 analyze_data.py file_path/file.h5
"""

import sys
import json

from classes.Case import Case

# Save file content to object.
input_file = sys.argv[1]
data = Case(input_file)

# Create JSON for mesh growth plot.
print('Creating JSON for mesh growth plot...')
data_export = {}
for i in range(len(data.times)):
    temp = {}
    temp['positions'] = data.positions[i].tolist()
    temp['types'] = data.types[i].tolist()
    temp['time'] = data.times[i]
    data_export['step_%d' % i] = temp

with open('cpp_implementation/results/plot_mesh.json', 'w') as outfile:
    json.dump(data_export, outfile)

# Create JSON for mesh growth and solution plot.
print('Creating JSON for mesh growth and solution plot...')
data_export = {}
for i in range(len(data.times)):
    temp = {}
    temp['positions'] = data.positions[i].tolist()
    temp['solution'] = data.solutions[i].tolist()
    temp['types'] = data.types[i].tolist()
    temp['time'] = data.times[i]
    data_export['step_%d' % i] = temp

with open('cpp_implementation/results/plot_mesh_solution.json', 'w') as outfile:
    json.dump(data_export, outfile)

print("Finished.")
